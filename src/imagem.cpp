#include <iostream>
#include "imagem.hpp"

using namespace std;

Imagem::Imagem() {
    setAltura(0);
    setLargura(0);
    setvalorMaximodeCor(0);
}

Imagem::~Imagem() {

}

void Imagem::setAltura(int altura) {
    this->altura = altura;
}

int Imagem::getAltura() {
    return altura;
}

void Imagem::setLargura(int largura) {
    this->largura = largura;
}

int Imagem::getLargura() {
    return largura;
}

void Imagem::setvalorMaximodeCor(int valorMaximodeCor) {
    this->valorMaximodeCor = valorMaximodeCor;
}

int Imagem::getValorMaximodeCor() {
    return valorMaximodeCor;
}

void Imagem::setMagico(string magico) {
    this->magico = magico;
}

string Imagem::getMagico() {
    return magico;
}