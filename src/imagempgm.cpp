#include <iostream>
#include <string>
#include "imagempgm.hpp"    

using namespace std;

Imagempgm::Imagempgm(string endereco, int posicaoInicial) {
    FILE *arquivo;
    char c;
    string texto = "";
    int linha = 0, countBite = 0;
    bool cabecalho = false;

    arquivo = fopen(endereco.c_str(), "rb");

    if (!arquivo) {
        cout << endl << "Arquivo não encontrado!" << endl;
        exit(0);
    } else {
        cout << "Arquivo encontrado." << endl;

        do {
            fscanf(arquivo, "%c", &c);
            countBite++;
            if (c == '#') {
                do {
                    fscanf(arquivo, "%c", &c);
                    countBite++;
                    if (c == '\n') {
                        texto = "";
                        break;
                    }
                } while (true);
            } else if (c == '\n' || c == ' ') {
                switch (linha) {
                    case 0:
                        Imagem::setMagico(texto);
                        texto = "";
                        break;
                    case 1:
                        Imagem::setLargura(atoi(texto.c_str()));
                        texto = "";
                        break;
                    case 2:
                        Imagem::setAltura(atoi(texto.c_str()));
                        texto = "";
                        break;
                    case 3:
                        Imagem::setvalorMaximodeCor(atoi(texto.c_str()));
                        texto = "";
                        cabecalho = true;
                        break;
                }
                linha++;
            } else {
                texto.append(&c);
            }
        } while (cabecalho == false);

        posicaoInicial = posicaoInicial + countBite;
        fseek(arquivo, posicaoInicial, SEEK_SET);
        do {
            fscanf(arquivo, "%c", &c);
            this->msgOculta.push_back(c);
        } while (!feof(arquivo));
    }
}

Imagempgm::~Imagempgm() {
}

void Imagempgm::gravarTXT() {
    FILE *arquivo;
    char caractereLido;
    int bit, j = 0;
    //string mensagem = "";

    arquivo = fopen("texto_oculto.txt", "w+b");

    cout << "entrou na funcao" << endl;
    caractereLido = 0x00;
    for (int i = 1; i <= 8; i++) {
        bit = this->msgOculta[j] & 0x01;
        caractereLido = (caractereLido << 1) | bit;
        j++;
    }
    cout << caractereLido;
    fprintf(arquivo, "%c", caractereLido);

    while (true) {
        caractereLido = 0x00;
        for (int i = 1; i <= 8; i++) {
            bit = this->msgOculta[j] & 0x01;
            caractereLido = (caractereLido << 1) | bit;
            j++;
        }
        if (caractereLido == '#') break;
        cout << caractereLido;
        fprintf(arquivo, "%c", caractereLido);
    }
    fclose(arquivo);
    cout << endl << ">>>Arquivo txt com a mensagem criado com sucesso." << endl;

}

void Imagempgm::setPosicaoInicial(int posicaoInicial) {
    this->posicaoInicial = posicaoInicial;
}

int Imagempgm::getPosicaoInicial() {
    return this->posicaoInicial;
}