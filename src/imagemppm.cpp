#include <iostream>
#include <string>
#include "imagemppm.hpp"

using namespace std;

Imagemppm::Imagemppm(string endereco) {
    FILE *arquivo;
    char c;
    string texto = "";
    int linha = 0;
    bool cabecalho = false;
    rgb pixel;

    arquivo = fopen(endereco.c_str(), "rb");

    if (!arquivo) {
        cout << endl << "Arquivo não encontrado!";
        exit(0);
    } else {
        cout << "Arquivo encontrado." << endl;

        do {
            fscanf(arquivo, "%c", &c);
            if (c == '#') {
                do {
                    fscanf(arquivo, "%c", &c);
                    if (c == '\n') {
                        texto = "";
                        break;
                    }
                } while (true);
            } else if (c == '\n' || c == ' ') {
                switch (linha) {
                    case 0:
                        Imagem::setMagico(texto);
                        texto = "";
                        break;
                    case 1:
                        Imagem::setLargura(atoi(texto.c_str()));
                        texto = "";
                        break;
                    case 2:
                        Imagem::setAltura(atoi(texto.c_str()));
                        texto = "";
                        break;
                    case 3:
                        Imagem::setvalorMaximodeCor(atoi(texto.c_str()));
                        texto = "";
                        cabecalho = true;
                        break;
                }
                linha++;
            } else {
                texto.append(&c);
            }
        } while (cabecalho == false);

        do {
            fscanf(arquivo, "%c", &pixel.red);
            fscanf(arquivo, "%c", &pixel.green);
            fscanf(arquivo, "%c", &pixel.blue);
            this->imagem.push_back(pixel);
        } while (!feof(arquivo));
    }
}

Imagemppm::~Imagemppm() {
}

void Imagemppm::filtroRed() {
    FILE *arquivo;
    arquivo = fopen("filtrored.ppm", "w+b");

    fprintf(arquivo, "%s\n", this->getMagico().c_str());
    fprintf(arquivo, "%d %d\n", this->getLargura(), this->getAltura());
    fprintf(arquivo, "%d\n", this->getValorMaximodeCor());

    for (int i = 0; i<this->imagem.size(); i++) {
        fprintf(arquivo, "%c%c%c", this->imagem[i].red, 0, 0);
    }
    fclose(arquivo);

}

void Imagemppm::filtroGreen() {
    FILE *arquivo;
    arquivo = fopen("filtrogreen.ppm", "w+b");

    fprintf(arquivo, "%s\n", this->getMagico().c_str());
    fprintf(arquivo, "%d %d\n", this->getLargura(), this->getAltura());
    fprintf(arquivo, "%d\n", this->getValorMaximodeCor());

    for (int i = 0; i<this->imagem.size(); i++) {
        fprintf(arquivo, "%c%c%c", 0, this->imagem[i].green, 0);
    }
    fclose(arquivo);

}

void Imagemppm::filtroBlue() {
    FILE *arquivo;
    arquivo = fopen("filtroblue.ppm", "w+b");

    fprintf(arquivo, "%s\n", this->getMagico().c_str());
    fprintf(arquivo, "%d %d\n", this->getLargura(), this->getAltura());
    fprintf(arquivo, "%d\n", this->getValorMaximodeCor());

    for (int i = 0; i<this->imagem.size(); i++) {
        fprintf(arquivo, "%c%c%c", 0, 0, this->imagem[i].blue);
    }
    fclose(arquivo);

}