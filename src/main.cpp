#include <iostream>
#include "imagemppm.hpp"
#include "imagempgm.hpp"
#include <string>

using namespace std;

int main() {
    string endereco;
    int op = 0, posicao;

    do {
        cout << endl <<
                "************************" << endl <<
                "1 - decifrar PPM\n" <<
                "2 - decifrar PGM\n" <<
                "3 - encerrar programa\n" <<
                "Escolha uma das opcoes: ";

        cin >> op;
        if (op == 1) {
            cout << "Digite o nome da imagem incluindo a extensao: ";
            cin >> endereco;
            Imagemppm *imagem = new Imagemppm(endereco);

            imagem->filtroRed();
            imagem->filtroGreen();
            imagem->filtroBlue();
            delete(imagem);
        } else if (op == 2) {
            cout << "Digite o nome da imagem incluindo a extensao: ";
            cin >> endereco;
            cout << "Digite a posicao inicial do texto: ";
            cin >> posicao;
            Imagempgm *imagem = new Imagempgm(endereco, posicao);

            imagem->gravarTXT();
            delete(imagem);
        } else if (op == 3)
            exit(0);
    } while (true);
    return 0;
}