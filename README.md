Exercício de programação número 1 da disciplina de Orientação a Objetos.
Para compilar o programa:
1-Abrir a pasta raiz do projeto através do terminal: 'ep1oo2016'
2-Executar o comando: 'make'

Para executar o programa:
1-Abrir a pasta através do terminal: 'ep1oo2016/bin"
2-Executar o comando: './main'
3-Seguir as instruções do programa para executar as funcoes.

Obs: as imagens devem estar na mesma pasta que o arquivo main for executado, neste caso na pasta bin. 
