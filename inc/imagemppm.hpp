#ifndef IMAGEMPPM_H
#define IMAGEMPPM_H

#include "imagem.hpp"
#include <vector>

#include <string>
#include <cstdlib>
#include <cstdio>

struct rgb {
    char red;
    char green;
    char blue;
};

class Imagemppm : public Imagem {
private:
    //vetor de pixels
    vector<rgb> imagem;    
public:
    //construtores
    Imagemppm(string endereco);
    ~Imagemppm();
    //metodos
    void filtroRed();
    void filtroGreen();
    void filtroBlue();
};
#endif