#ifndef IMAGEM_H
#define IMAGEM_H

using namespace std;

class Imagem {
    //atributos
private:
    string magico;
    int altura;
    int largura;
    int valorMaximodeCor;

public:
    //construtores
    Imagem();
    ~Imagem();

    //acessores
    void setAltura(int altura);
    int getAltura();
    void setLargura(int largura);
    int getLargura();
    void setvalorMaximodeCor(int valorMaximodeCor);
    int getValorMaximodeCor();
    void setMagico(string magico);
    string getMagico();
};

#endif