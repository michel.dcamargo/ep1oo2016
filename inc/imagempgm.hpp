#ifndef IMAGEMPGM_H
#define IMAGEMPGM_H

#include "imagem.hpp"
#include <vector>

#include <string>
#include <cstdlib>
#include <cstdio>

class Imagempgm : public Imagem {
private:
    int posicaoInicial;
    vector<char> msgOculta;
public:

    //construtores
    Imagempgm(string endereco, int posicaoInicial);
    ~Imagempgm();
    //metodos
    void setPosicaoInicial(int posicaoInicial);
    int getPosicaoInicial();
    void gravarTXT();
};
#endif